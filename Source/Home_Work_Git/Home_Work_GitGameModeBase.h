// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Home_Work_GitGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOME_WORK_GIT_API AHome_Work_GitGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
